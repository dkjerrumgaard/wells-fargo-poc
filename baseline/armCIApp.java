import java.lang.Long;

import org.apache.pig.data.Tuple;
import org.apache.pig.data.TupleFactory;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.apache.pig.EvalFunc;
import org.apache.pig.data.DataType;
import org.apache.pig.data.Tuple;
import org.apache.pig.impl.logicalLayer.schema.Schema;

public class armCIApp extends EvalFunc<Tuple> {
	
	// application specific source fields
	private final List<String> plfList = Arrays.asList(new String[] {
			"RANK_NUM",
			"AD_ID",
			"ECN",
			"ECN_NAME",
			"RESP_DT",
			"ATM_LD_DT",
			"CNF_AUD_INSRT_DT_TM_PRTN"}); 
			
	protected final String[] ciStdXrefField = new String[] {
			"SOR_APPL_ID",
			"FILE_NAME",
			"FIELD_NAME",
			"LOW_RANGE",
			"HIGH_RANGE",
			"TARGET_TEXT",
			"TARGET_DESCRIPTION",
			"EFFV_DT",
			"EXPR_DT"
	};
	
	protected HashMap<Integer, HashMap<String, String>> ciStdXrefMap = null;
	private final String SOR_CMPTR_APPL_ID = "ARM";

    long ECN = 0L; 
	
	public Tuple exec(Tuple input) throws IOException {
		if (input.size() != plfList.size()) 
			throw new RuntimeException("[WF] Wrong arguments received into armCIApp. Expected " + plfList.size() + " arguments...tuple received is: [" + input + "]");
		
		if(ciStdXrefMap == null) ciStdXrefMap = populateLookupMap("t_ci_std_xref", ciStdXrefField);
		
		Tuple output = null;
		try {
			output = TupleFactory.getInstance().newTuple(9);
			
			String CNF_ACTVY_DT = input.get(plfList.indexOf("RESP_DT")).toString().trim();
			String DERV_APPL_DESC = populateDervApplDesc("sor_cmptr_appl_id", SOR_CMPTR_APPL_ID, CNF_ACTVY_DT);
			String inECN = input.get(plfList.indexOf("ECN")).toString().trim();
			ECN = Long.parseLong(inECN);
			
			String ECN_NAME = null; if (input.get(plfList.indexOf("ECN_NAME")) != null) {ECN_NAME = input.get(plfList.indexOf("ECN_NAME")).toString().trim();}
			
			long OCCUR_NUM =  (Long)input.get(plfList.indexOf("RANK_NUM"));
			String SOR_KEY = input.get(plfList.indexOf("AD_ID")).toString().trim();
			String CNF_AUD_INSRT_DT_TM = input.get(plfList.indexOf("CNF_AUD_INSRT_DT_TM_PRTN")).toString();

			// populate ACTVY fields
			
			output.set(0, CNF_ACTVY_DT);
			output.set(1, populateDervApplDesc("sor_cmptr_appl_id", SOR_CMPTR_APPL_ID, CNF_ACTVY_DT)); 
			output.set(2, ECN);
			output.set(3, ECN_NAME);
			output.set(4, OCCUR_NUM);
			output.set(5, SOR_CMPTR_APPL_ID);
			output.set(6, SOR_KEY);
			output.set(7, populateAudInsrtDtTm(input.get(plfList.indexOf("CNF_AUD_INSRT_DT_TM_PRTN")).toString())); 
			output.set(8, popultePrtnField(CNF_ACTVY_DT, CNF_AUD_INSRT_DT_TM, "arm"));
		} catch (Exception e) {
			throw new IOException("[WF] Caught exception processing input row of ATM Dispositions: " + input, e);
		}
		return output;
	}

	/*
	 * 	PERFORM LOOK-UP TO CI_STD_XREF_LKP ON
		SOR_APPL_ID = 'CI'
		FILE_NAME = 'ACTVY'
		FIELD_NAME = 'SOR_CMPTR_APPL_ID'
		LOW_RANGE = SOR_CMPTR_APPL_ID
		DATE PORTION OF SOR_ACTVY_DT_TM IS BETWEEN EFFV_DT AND EXPR_DT INCLUSIVE
		MOVE TRGET_TEXT
		IF NO MATCH FOUND, MOVE 'NOT FOUND'
	 */
	
	protected String populateDervApplDesc(String sorFieldName, String sorLowRange, String cnfActvyDt) throws IOException{
		String dervApplDesc = "NOT FOUND";
		HashMap<Integer, HashMap<String, String>> lm = ciStdXrefMap;
		for (Map.Entry<Integer, HashMap<String, String>> okv : lm.entrySet()) {
			if ( okv.getValue().get("SOR_APPL_ID").trim().equalsIgnoreCase("CI") 
					&& okv.getValue().get("FILE_NAME").trim().equalsIgnoreCase("ACTVY")
					&& okv.getValue().get("FIELD_NAME").trim().equalsIgnoreCase(sorFieldName)
					&& okv.getValue().get("LOW_RANGE").trim().equalsIgnoreCase(sorLowRange)
					&& isValidDate(cnfActvyDt,
							okv.getValue().get("EFFV_DT"), 
							okv.getValue().get("EXPR_DT"))) {
							dervApplDesc = okv.getValue().get("TARGET_TEXT");
							break;
			}
		}
		return dervApplDesc;
	}
	
		private boolean isValidDate(String checkDateStr, String fromDateStr, String toDateStr) {
		SimpleDateFormat inputDateFormat = new SimpleDateFormat("yyyy-MM-dd");

		Date checkDate = null;
		Date fromDate = null;
		Date toDate = null;

		try {
			checkDate = inputDateFormat.parse(checkDateStr);
			fromDate = inputDateFormat.parse(fromDateStr);
			toDate = inputDateFormat.parse(toDateStr);
		} catch (ParseException pe) {
			return false;
		}
		return (checkDate.compareTo(fromDate) >= 0) && (checkDate.compareTo(toDate) <= 0);
	}
	
	protected String populateAudInsrtDtTm(String audInsrtDtTmInput) throws IOException, ParseException {
		SimpleDateFormat audInsrtDtTmInputFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
		SimpleDateFormat audInsrtDtTmDispFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return audInsrtDtTmDispFormat.format(audInsrtDtTmInputFormat.parse(audInsrtDtTmInput));
	}
	
	protected String popultePrtnField(String cnfActvyDt, String cnfAudInsrtDtTmPrtn, String cnfCmptrApplCd) throws IOException {
		return cnfActvyDt.replace('-', '_') + "^" + cnfCmptrApplCd.toLowerCase() + "_" + cnfAudInsrtDtTmPrtn.replace('-', '_') ;
	}
	
	public List<String> getCacheFiles() {
		List<String> list = new ArrayList<String>(4);
		list.add("/mat/sow_xref/t_ci_std_xref#t_ci_std_xref");
		return list;
	}
	
	public Schema outputSchema(Schema input){
		Schema actvySchema = null;
		try {
			Schema tupleSchema = new Schema();
			tupleSchema.add(new Schema.FieldSchema("cnf_actvy_dt_tm", DataType.DATETIME));
			tupleSchema.add(new Schema.FieldSchema("derv_appl_desc", DataType.CHARARRAY));
			tupleSchema.add(new Schema.FieldSchema("ecn", DataType.LONG));
			tupleSchema.add(new Schema.FieldSchema("ecn_name", DataType.CHARARRAY));
			tupleSchema.add(new Schema.FieldSchema("occur_num", DataType.INTEGER));
			tupleSchema.add(new Schema.FieldSchema("sor_cmptr_appl_id", DataType.CHARARRAY));
			tupleSchema.add(new Schema.FieldSchema("sor_key", DataType.CHARARRAY));
			tupleSchema.add(new Schema.FieldSchema("aud_insrt_dt_tm", DataType.DATETIME));
			tupleSchema.add(new Schema.FieldSchema("prtn_field", DataType.CHARARRAY));
			
			actvySchema = new Schema(new Schema.FieldSchema("t_actvy", tupleSchema, DataType.TUPLE));
		}catch (Exception e) {
			throw new RuntimeException("[WF] Exception in output schema.", e);
		}
		return actvySchema;
	}
	
	protected HashMap<Integer, HashMap<String, String>> populateLookupMap(String fileToRead, String[] colHeader) throws IOException {
		HashMap<String, String> lookupInnerMap = null;
		int lookupIndex = 0;
		String record;
		String[] values = new String[50];
		HashMap<Integer, HashMap<String, String>> populatedMap = new HashMap<Integer, HashMap<String, String>>();
		FileReader fReader = new FileReader("./"+fileToRead);
		BufferedReader bReader = new BufferedReader(fReader);

		while ((record = bReader.readLine()) != null) {
			// split based on delimiter on the lookup file
			values = record.split("\t");
			lookupInnerMap = new HashMap<String, String>();
			for (int i = 0; i < values.length; i++) {
				// add to inner map
				lookupInnerMap.put(colHeader[i], values[i].trim());
			}
			// add to outer map
			populatedMap.put(new Integer(lookupIndex), lookupInnerMap);
			lookupIndex++;
		}
		bReader.close();
		return populatedMap;
	}
	
	
}
