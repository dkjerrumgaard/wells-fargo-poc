set job.name arm; 
set output.compression.enabled true;
set output.compression.codec org.apache.hadoop.io.compress.BZip2Codec;

Register arm.jar;
Register /usr/hdp/2.2.0.0-1947/pig/piggybank.jar;

atm_dispositions_all = LOAD 'mat.sow_arm_feed' USING org.apache.hive.hcatalog.pig.HCatLoader();
atm_dispositions = FILTER atm_dispositions_all BY (atm_ld_dt >= '$fromdate') and (atm_ld_dt <= '$todate');

ecn_names = LOAD 'mat.sow_ecn_name' USING org.apache.hive.hcatalog.pig.HCatLoader();

full_arm = JOIN atm_dispositions by ecn left outer, ecn_names by ecn;
  
rank_arm = RANK full_arm;

out_arm = FOREACH rank_arm { 

  conforms = armCIApp(rank_full_arm, ad_id, atm_dispositions::ecn, ecn_name, resp_dt, atm_ld_dt, '$cnfActvyDtTm4Prtn');

  GENERATE FLATTEN(conforms);
	
  }
  
DESCRIBE out_arm;	
  
  -- add back the parallel 30 - to simulate small files issue
  
STORE out_arm INTO '/mat/dan_test_sow/pig'        USING org.apache.pig.piggybank.storage.MultiStorage('/mat/dan_test_sow/pig',  '8', 'bz2', '\u0001');
