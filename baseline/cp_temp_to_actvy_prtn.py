#!/usr/bin/env python
import subprocess
import os
import time
import sys

if(len(sys.argv) == 3):
        app_temp_cnf_res_dir = str(sys.argv[1])
        table_base_dir = str(sys.argv[2])
        local_process = subprocess.Popen(['hadoop', 'fs', '-lsr', ''+app_temp_cnf_res_dir+'/*'], stdout=subprocess.PIPE)
        dir_list = local_process.stdout
        print "Copying files from temp dir, "+app_temp_cnf_res_dir

        for f in dir_list:
                isdir = f.split()[0]
                if isdir.find('d')==-1:
                        file_name = f.split()[7]
                        full_name = f.split('pig/')[1]
                        hive_part = full_name.split('^')
                        dt = str(hive_part[0]).replace('_','/')
                        dest_file_name = str(hive_part[2]).replace('\n', '');
                        moveProcess = subprocess.Popen(['hadoop', 'fs', '-cp', file_name, table_base_dir+'/'+dt+'/'+ dest_file_name], stdout=subprocess.PIPE)
                        print 'Copied ' + file_name + ' to ' + table_base_dir+'/'+dt+'/'+ dest_file_name
else:
        print 'Usage: <file-name> <src-dir> <dest-base-dir>'
        print 'Example: cp_temp_to_actvy_prtn.py /mat/prspt/temp/t_actvy_no_ecn/pig /mat/t_actvy_no_ecn'